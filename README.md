# super-editor-2015

## Table of Contents

* [Editor](#editor)
* [Tilesets](#tilesets)
* [Layers](#layers)
* [Objects](#objects)
* [Selection and Movement](#selection-and-movement)
* [Copy Cut Delete Paste Und](#copy-cut-delete-paste-undo)
* [Load](#load)
* [Save](#save)
* [Object Properties](#object-properties)

## Editor

* to open the editor press `F3`
* to open the StartArea map in the game press `F5`
* to open in the editor the map that was previously being played in the game, press `F2`
* to open in the game the map that was previously saved or loaded in the editor, press `F6`

## Tilesets

* to choose a tileset press `T + <tileset number>`
* if there are more than 10 tilesets, press `T + <tileset number> + ENTER`
* to unselect a tileset press `T + 0 + 0`
* after a tileset is chosen you should be able to select each tile and place it on the tile grid to create your maps
* to unselect a chosen tile press `ESC`

![tileset](gifs/tileset.gif)

## Layers

* to choose a layer press `E + <layer number> + ENTER`
* to activate or deactivate a layer press `E + <layer number> + SPACE`, deactivated layers are not drawn
* by default all layers are deactivated, so if you try to place tiles and it doesn't show up, you probably don't have a layer selected

![layer](gifs/layer.gif)

## Objects

* to choose an object press `B + <object number>`, 
* to spawn an object, after choosing it press `LEFT CLICK`
* to spawn multiple objects hold `ALT` before pressing `LEFT CLICK`
* some objects are single squares that can be spawned by left clicking, others let you define a shape
* to snap the mouse to the grid hold `SHIFT`
* to unselect a chosen object press `ESC`

![object](gifs/object.gif)

## Selection and Movement

* to select an object press `LEFT CLICK` while hovering it
* to select multiple objects hold `CTRL`
* to select an area press `S` and drag the mouse
* to exit selection mode press `ESC`
* to move a selected object `LEFT CLICK` and drag it around
* to move multiple selected objects hold `CTRL`
* to move a selected object while snapped to the grid, drag it around while holding `SHIFT`, sometimes it bugs a little, so just select the object (without dragging) and hold shift and it should correct itself
* to unselect an object press `LEFT CLICK` while not hovering it

![select](gifs/select.gif)

## Copy Cut Delete Paste Undo

Copy, cut, paste and delete should be straight forward. They act upon selected objects like you'd expect them to. As for undo it's just a normal undo. It works most of the time for most actions, but I may have forgotten a few.

## Load

* to choose a map to be loaded press `L + <map number>`
* to load the chosen map press `CTRL + L`

![load](gifs/load.gif)

## Save

* change the textinput box with the name of the map you wanna save, all maps are overriden by default
* to save the current map to the name on the textinput box press `CTRL + S`

## Object Properties

* to change an object's properties `RIGHT CLICK` while hovering it

![prop](gifs/prop.gif)

The editor already comes with two maps: `StartArea` and `StartArea2`. On those maps there's an example of object properties being used as the `LevelTransition` objects have their `target_level` properties set to point to the other map. So for this case that property enables the player to go through levels whenever it comes in contact with those `LevelTransition` objects. They will certainly be used for other things in the future.

### Currently valid properties

| Object | Property | Value |
| :----- | :------- | :---- |
| LevelTransition | target_level | the name of the map to transition to |
